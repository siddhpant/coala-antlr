coala-antlr
-----------

This repository houses coantlib and coant-bears which are an extension to 
coala and integrate ANTLRv4 into coala to enable parse tree based bears.
