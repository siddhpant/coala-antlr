set -e
cd "${0%/*}"
mkdir grammars
curl https://raw.githubusercontent.com/antlr/grammars-v4/master/python3-py/Python3.g4 > grammars/Python3.g4
curl https://raw.githubusercontent.com/antlr/grammars-v4/master/xml/XMLLexer.g4 > grammars/XMLLexer.g4
curl https://raw.githubusercontent.com/antlr/grammars-v4/master/xml/XMLParser.g4 > grammars/XMLParser.g4
